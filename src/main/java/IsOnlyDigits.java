import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class IsOnlyDigits extends TypeSafeMatcher<String> {

    @Override
    protected boolean matchesSafely(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException nfe){
            return false;
        }
    }

    public static Matcher<String> onlyDigits() {
        return new IsOnlyDigits();
    }

    @Override
    public void describeTo(org.hamcrest.Description description) {
        description.appendText("only digits");
    }
}