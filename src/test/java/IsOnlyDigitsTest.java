import org.junit.Test;

import static org.junit.Assert.*;

public class IsOnlyDigitsTest {

    @Test
    public void givenAString_whenIsOnlyDigits_thenCorrect() {
        String digits = "1234";

        assertThat(digits, IsOnlyDigits.onlyDigits());
    }
}